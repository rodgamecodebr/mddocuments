Nice to meet you Akinori.

1. My recent project is fully developed by myself so I am the main programmer. My main goal was to have a third-person combat game loop working as a small but complete game. 

   So basically these are the functionality I had in mind for the demo and I marked with **OK** what is done and **NO** what needs to be worked:

   - ### Retargeting character animations of different skeletons 

     I learned to setup myself mainly the combo montage with different sections where each section is triggered.

     Main Character and NPC animations
     (walk, run, jump, simple attacks, combo montage attack) **OK**

   - ### Input Controls

     WASD or Joypad Character Locomotion **OK**

     Simple Map Buttons Attacks Assigned **OK**

     Special Button to perform Gameplay Ability System Combo (Level ranges from 1 to 5) **OK**

     Camera Control Default using Right ThumbStick **OK**

   - ### Characters (C++ and Blueprints)

     Both Main and NPC character inherits from C++ Base Class (NinjaCharacter) and have the Ability System Interface to be implemented accordingly (When to activate and acquire levels)
     **OnDamaged** implementable event where the Blueprint class will implement the response to combo attacks coming from other characters **OK**

     **GameplayAbilities** array to store the different ability levels **OK**

     **GameplayEffect** array to store the effects that are going to be applied by the system **NO**

     **Attribute Sets** to store properties such as health, level values not wired up yet **NO**

     #### Collision

     Each character has collision body parts to register overlap events for combo attacks and other interactions. 
     
     **Tags** are assigned to characters and objects so that overlap events that triggers different events from player. **OK**
     
     **OnComponentBeginOverlap** listens to NPC characters when hitting or being hit.
     
     ### Camera System
     
     The camera system has the objective to position and orient the camera around the player.
     
     **Right Thumb Stick Adjust** - Uses the default behavior of camera positioning and orientation that comes with the Third Person Template **OK**
     
     **360 Camera Movement** - offsets a certain distance from player and rotates around the character viewing it in the center **OK**
     
     **Follow Path Movement** - when triggered the camera follows a path predefined movement and orientation using values from Sequencer. It adapts its location and orientation using the character location as reference constrainer. **NEED FIXES** 
     
     ### Combo System (Blueprint)
     
     When starting the game, the combo system activates gameplay abilities for characters depending on ability level which it is currently in. 
     
     **Time Period** - there is time period that the combo animation must be performed fully or else it starts from the beginning. **OK**
     
     **Attack Force** - each animation from combo attack has a force value which affect on how the character being hit deals with. **OK**
     
     **Jump Section Notifies** - each section of the combo has anim notify state blueprint checking which section it is in, if the player activates the next section of animation to be played **OK**
     
     **Combo Damage Notifies** - each section identifies which type of animation it is (in this case, could be hand animation or foot animation) and assigns a value to represent damage value to send to hit character. **OK**
     
     ### UI System (Blueprint)
     
     Main Menu 
     
     ​	**UI UMG Logo** -  Fade in animation triggering dispatcher events when ending. **OK**
     
     ​	**Press Key Command** - listens for any key input to start the game **OK**
     
     ### Physics System (CHAOS)
     
     **Die Ragdoll Activaction**- **NO**
     
     **Destructible Objects** -**NO**

