# C++ Concurrency

### Introduction

A system performing multiple independent activities in parallel, rather than sequentially.

A desktop computer have had a one processor, with a single unit processing core, can only perform one task at a time, but it can switch between tasks many times per second. This is called **task switching**. The task switching provides the illusion of concurrency to the user.

Computers containing multiple processors have been used for servers and high performance computing tasks for years. These computers are capable of running more than one task in parallel. This is called **hardware concurrency.**

Hardware threads is the measure of how many independents tasks the hardware can genuinely run concurrently.

The use of concurrency in applications may well depend of the amount of hardware concurrency available.

Some processors can execute multiple threads on a single core.

**There are some approaches to concurrency applications**. One is to have multiple single threaded processes, and the second approach is to have multiple threads in a single process. Also, they can be combined and have multiple processes, some of which are multithreaded and some of which are single-threaded.

#### First approach

Divide application into:

- Multiple
- Separate
- Single threaded process
- Run at the same time
- Pass messages to each other through interprocess communications (signals, sockets, files, etc)
- One downside is communication between process is complicated to set up or slow, OS may provide a lot of protection between processes to avoid each other from modifying data belonging to each other.
- Easier to write safe concurrent code rather than threads
- Can run separate processes on distinct machines connected over a network

#### Second Approach

- Multiple threads
- Single process
- Each thread runs independently of the others
- May run a different sequence of instructions
- Share the same address space
- Most data can be accessed directly from all threads
- Lack of protection data between threads
- Overhead associated with multiple threads much smaller
- Ensure the data is consistent when accessed by multiple threads

C++ doesn't provide support for communication between process so the second approach is the most widely used for that language.

#### Why use concurrency?

- Separation of concerns
- Performance

#### Thread

#### Process



### Hello World

```
#include <iostream>
#include <thread>
void hello()
{
    std::cout<<"Hello Concurrent World\n";
}
int main()
{
    std::thread t(hello);   3
    t.join();
}
```

thread header that contains classes and functions for managing threads

Every thread has to have an initial function, where the new thread execution begins.

Has to be declared with the constructor of thread object.

join causes the calling thread to wait for the thread associated

detach causes not to wait for the thread to finish