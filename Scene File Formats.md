# Scene File Formats

### Open Data Description Language (OpenDDL)

General purpose, human-readable, and strongly-typed data language for information exchange.

```
Point
{
   float {1.0, 2.0, 3.0}
}
```

```
Point $position
{
   float {1.0, 2.0, 3.0}
}
```

```
ref {$position}
```

### Data Types

| `bool`, `b`                     | A boolean type that can have the value `true` or `false`.    |
| ------------------------------- | ------------------------------------------------------------ |
| `int8`, `i8`                    | An 8-bit signed integer that can have values in the range [−27, 27 − 1]. |
| `int16`, `i16`                  | A 16-bit signed integer that can have values in the range [−215, 215 − 1]. |
| `int32`, `i32`                  | A 32-bit signed integer that can have values in the range [−231, 231 − 1]. |
| `int64`, `i64`                  | A 64-bit signed integer that can have values in the range [−263, 263 − 1]. |
| `uint8`, `u8`                   | An 8-bit unsigned integer that can have values in the range [0, 28 − 1]. |
| `uint16`, `u16`                 | A 16-bit unsigned integer that can have values in the range [0, 216 − 1]. |
| `uint32`, `u32`                 | A 32-bit unsigned integer that can have values in the range [0, 232 − 1]. |
| `uint64`, `u64`                 | A 64-bit unsigned integer that can have values in the range [0, 264 − 1]. |
| `half`, `float16`, `h`, `f16`   | A 16-bit floating-point type conforming to the standard S1E5M10 format. |
| `float`, `float32`, `f`, `f32`  | A 32-bit floating-point type conforming to the standard S1E8M23 format. |
| `double`, `float64`, `d`, `f64` | A 64-bit floating-point type conforming to the standard S1E11M52 format. |
| `string`, `s`                   | A double-quoted character string with contents encoded in [UTF-8](https://en.wikipedia.org/wiki/UTF-8). |
| `ref`, `r`                      | A sequence of structure names, or the keyword `null`.        |
| `type`, `t`                     | A type whose values are identifiers naming types in the first column of this table. |
| `base64`, `z`                   | Generic binary data encoded as [Base64](https://en.wikipedia.org/wiki/Base64). |

