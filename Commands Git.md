## Commands Git

### Pull request in a branch

git push --force origin 

head:refs/heads/feature/branchname

### Rebase under branch

git  checkout origin/develop

git rebase origin/develop

### Remove file from commit

git reset head~1 filename

git commit --amend --no-edit

### Reset master branch to current HEAD

git checkout -B master @

git push

### Move unpushed commits to new branch

git branch new-branch-name

git push origin new-branch-name

git reset --hard origin/old-branch-name

### Change from Http to SSH url

 git remote set-url origin 

git@github.com:USERNAME/REPOSITORY.git 

### .gitignore being ignored

git rm -r --cached

git add .

git commit -m "fixed untracked files"

### Rename a local branch

While pointed to any branch

git branch -m <oldname> <newname>

Rename the current branch

git branch -m <newname>

### Edit commit message

If commit is the last commit

git commit --amend

If commit is not the last commit

git rebase -i HEAD~
=======
