# Android Programming

### Componentes de Aplicativo

#### Atividade

É o ponto de entrada para a interação com o usuário.

Você implementa uma atividade como uma subclasse da classe **Activity**.

#### Serviço

O serviço é um ponto de entrada para manter um aplicativo em execução no segundo plano, seja qual for o motivo. É um componente executado em segundo plano para realizar operações de execução longa ou trabalho para processos remotos.

Serviços não apresentam uma interface do usuário.

Serviços são implementados como uma subclasse de **Service**.

#### Broadcast Receivers

É um componente que faz o sistema entregar eventos ao aplicativo fora de fluxo de usuários comuns.

#### Provedores de Conteudo

Gerenciam um conjunto compartilhado de dados do aplicativo que voce pode armazenar nos sistemas de arquivos, em banco de dados, na web, ou qualquer local de armazenamento persistente acessivel ao seu aplicativo.

#### Ativacao de Componentes

Atividades, servicos e broadcast receivers sao ativados por uma mensagem assincrona chamada **intent**. Pense neles como mensageiros que solicitam uma acao de outros componentes, seja o componente pertencente ao aplicativo ou nao.

O intent é criado com um objeto **Intent** que define uma mensagem para ativar um componente especificou um tipo especifico de componente.

#### Intent

é um objeto de mensagem que pode ser usado para solicitar uma ação de outro componente do aplicativo.

Fundamentais uso:

- Como iniciar uma atividade
- Como iniciar um serviço
- Como fornecer uma transmissão

#### Arquivo de Manifesto

Antes de o sistema Android iniciar um componente de aplicativo, é preciso ler o arquivo de manifesto AndroidManifest.xml do aplicativo para que o sistema saiba que o componente existe.

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest ... >
    <application android:icon="@drawable/app_icon.png" ... >
        <activity android:name="com.example.project.ExampleActivity"
                  android:label="@string/example_label" ... >
        </activity>
        ...
    </application>
</manifest>
```

O manifesto faz:

- identifica as permisões do usuário de que o aplicativo precisa.
- declara o nivel de API minimo exigido pelo aplicativo
- declara os recursos de hardware e software usados ou exigidos pelo aplicativo
- declara as bibliotecas de api de que o aplicativo precisa para ser vinculado

A principal tarefa do manifesto é informar ao sistema os componentes do aplicativo.

<activity> para atividades

<service> para serviços

<receiver> para broadcast receivers

<provider> para provedores de conteudo

Para o sistema identificar os componentes que podem responder a um intent, compara-se o intent recebido com os filtros de intent fornecidos no arquivo de manifesto de outros aplicativos no dispositivo.

Para declarar um filtro de intent no componente, adiciona-se um elemento <intent_filter> como filho do elemento de declaração do componente.

```xml
<manifest ... >
    ...
    <application ... >
        <activity android:name="com.example.project.ComposeEmailActivity">
            <intent-filter>
                <action android:name="android.intent.action.SEND" />
                <data android:type="*/*" />
                <category android:name="android.intent.category.DEFAULT" />
            </intent-filter>
        </activity>
    </application>
</manifest>
```

### Visão Geral de Projetos

Um projeto no Android Studio contem tudo o que define seu espaço de trabalho para um app, do código fonte e recursos ao código de teste e configurações da compilação.

#### Módulos

é uma coleção de arquivos de origem e configurações da compilação que permite dividir o projeto em unidades distintas de funcionalidade. O projeto pode ter um ou mais módulos, e um módulo pode usar outro como dependência.

##### Modulo de App

##### Modulo do recurso

##### Modulo de biblioteca