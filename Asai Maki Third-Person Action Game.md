# Asai Maki Project

Developed with Unreal Engine 4. It is a game application featuring third-person character mechanics gameplay as a demonstrated proof of skills using the engine.

## Installation

Download from this repository link the source code of the project.

## Version

Made with Unreal Engine 4.24.3

## Features

- Retargeting animation characters of different skeletons
- C++ and Blueprint communication
- UI UMG
- Chaos physics
- NPC Behavior tree
- Character Ability System