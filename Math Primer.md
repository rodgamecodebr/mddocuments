# Math Primer

### Real Numbers

Integers

Rational Numbers

Irrational Numbers

Real Numbers

Everything in a computer game is represented by numbers.

Computers are restricted to using a small range of numbers that have a finite number of digits. This excludes all irrational numbers and many rational numbers.

Intervals - Start and End (0,1) or [0,1]

Absolute Value of a Real Number - distance from 0 in number line where positive numbers fall to the right of 0 and negative numbers fall to the left of 0

Properties

- Closure, a+b is a real number
- Commutative, a+b=b+a and ab=ba
- Associative, (a+b) + c = a + (b+c) and (ab)c = a(bc)
- Identity, a+0 = 0+a = a and a*1=1*a = a
- Inverse, a+(-a) = 0 and a(1/a)=(1/a)a = 1
- Distributive, a(b+c) = ab + ac

### Equations

Is a statement claiming equality between two numbers or expressions.

5+2 = 7 claims that 5 plus 2 is equal to 7.

Properties

- Reflexive, a = a
- Symmetric, a=b then b=a
- Transitive, a=b then b=c then a=c
- Substitution, a=b then in an expression where a appears can be replaced with b

Operations

- Addition, a+b, sum, a and b are terms of the equation
- Subtraction, a+(-b), the difference of a and b, -a is the additive inverse of a
- Multiplication, a*b, product,  a and b are the factors of equation, 1/a is the multiplicative inverse of a
- Division, a/b, quotient or ratio a is the numerator and b the denominator equivalent to the product of a and the multiplicative inverse of b

Order of operations

First perform all multiplications and divisions, reading the expression from left to right, then perform all additions and subtractions.

Parentheses override the order of operation rules.

### Fractions

is one integer divided by another non zero integer. a/b

Properties

- Equality
  $$
  \frac{a}{b}=\frac{c}{d}
  $$

- Equivalence,  
  $$
  \frac{a}{b}=\frac{ad}{bd},  d != 0
  $$
  
- Addition, 
  $$
  \frac{a}{b}+\frac{c}{b}=\frac{a+c}{b}
  $$
  
- Subtraction, 
  $$
  \frac{a}{b}-\frac{c}{b}=\frac{a-c}{b}
  $$
  
- Multiplication, 
  $$
  \frac{a}{b}*\frac{d}{c}=\frac{ad}{bc}, c!=0
  $$
  
- Division, 
  $$
  \frac{a}{b}/\frac{d}{c}=\frac{ac}{bd}, c!=0
  $$
  
- Sign Properties, 
  $$
  \frac{0}{b}=0, b!=0
  $$
  
- Zero Properties, 
  $$
  \frac{a}{0}
  $$
  is undefined.

### Exponents

is a compact way of writing repeated multiplication.
$$
a^n
$$

$$
a^0=1
$$

$$
a^1=a
$$

$$
a^b a^c=a^{b+c}
$$

$$
(a^b)^c=a^{bc}
$$

$$
(a)^{-b}=\frac{1}{a^b}
$$

### Algebra

Variable

is simply a symbol, such as x, which can take any number of values.

Variables are used to form equations.

Equations and Their solutions

is a statement that asserts the equality of two expressions.

Solving Single Variable Equations

Solving Multi-Variable Equations

System of Linear Equations

​	is a equation that consists of a constant real number multiplied by a variable that is raised to the first power.

#### Simplifying and Rearranging equations

- Collecting like terms , 
  $$
  3+3 = (2)(3)
  $$
  
- Factoring, 
  $$
  x+yx = 3 => x(1+y) = 3
  $$
  , Apply the distributive rule in reverse.

- Expanding, 
  $$
  (a+b)(c+d) = ac + bc + ad + bd
  $$
  

### Functions

A set is an unordered collection of items.

A rule that associates every element of a set A with exactly one element of a set B.

Usually denoted by a single, lower case letters of the alphabet, f,g,h.

Functional notation f(x) is the range, x is the domain
$$
f(x)=x^2
$$

Rule. The rule of this function associates the real number with the real number x.

Notation. 
$$
f(5)=25
$$

$$
f(3)=9
$$

Domain. The domain of this function is the set of all real numbers. No matter that number you pick, the function associates some other number with it.

Range. The range of this function is the set of all real numbers greater than or equal to zero.

### Non- Functions

​	any rule that associates an element in the domain with two or more elements in the range is not a function.

### Inverse Functions

​	an inverse function undoes the operation of a function.
$$
f(x)=x+1
$$
​	The inverse:
$$
f^-1(x)=\frac{1}{x+1}
$$
​	Let's take a real example:
$$
f(3)=-1 
$$
then the inverse becomes:
$$
f^-1(-1)=3
$$
​	not all functions have inverses. If a function f associates two different items in the domain with a single item in the range, then it doesn't have an inverse. For example:
$$
f(x)=x^2
$$

$$
f(2)=2^2=4
$$

$$
f(-2)=-2^2=4
$$

$$
f^-1(x) = \frac{1}{x^2}
$$

$$
f^-1(2)=\frac{1}{2^2}=\frac{1}{4}
$$

$$
f^-1(-2)=\frac{1}{-2^2}=\frac{1}{4}
$$

### Common Mathematical Functions

#### The Square Root Function

​	Positive Square Root Function:
$$
+\sqrt(x)
$$
​	Negative Square Root Function:
$$
-\sqrt(x)
$$

#### Exponential Function

$$
exp(x)=e^x
$$

​																	where
$$
e=2.718281828459045
$$
​											known as the **natural exponent**.

#### Logarithmic Function

​	The exponential function has an inverse, it is denoted by
$$
ln(x)
$$
​														the natural log of x.

​	Each logarithmic function is uniquely identified by its base, which, in the case of the natural log is equal to e.

​	The logarithmic function with base b, denoted 
$$
log_b(x)
$$
is defined to be a number y such that 
$$
b^y=x
$$

### Solving quadratic with the Square Root Functions

An equation in the form of
$$
ax^2+bx+c
$$
x being the variable and a,b,c real numbers is called a **quadratic equation**.

Two real numbers satisfy a quadratic equation.
$$
x1=\frac{-b+\sqrt(b^2-4ac)}{2a}
$$

$$
x2 = \frac{-b-\sqrt(b^2-4ac)}{2a}
$$

### Geometry

#### 	Measure of Points

​		In game development, the most common way to measure points i by using the Cartesian coordinate systems.

In the Cartesian coordinate system, points are measured by describing their location on imaginary rulers , known as axes. 

 - 1 dimension
 - 2 dimensions
 - 3 dimensions

#### Graphing Functions and Equations

The CCS can be used to graph a function- that is, to display a graphical representation of the function.

All points of the form 
$$
(x,f(x))
$$

$$
(x,y,f(x))
$$

2D and 3D points respectively.

#### Equation of Geometric Objects

There are handful extremely important equations that come up all the time in computer game development. The first such equation if the **slope-intercept** of a two-dimensional line.

#### Slope-Intercept

gives you the coordinate y lying on any x coordinate on the line.
$$
y=mx+b
$$
The constant m is called the slope of the line and it is precisely equal to the number of vertical units per horizontal unit. The slope measures the elevation of the line.

The constant b is called the y-intercept of the line, since it gives you the y value when x=0.

#### General Form of 2D Line

The second important equation is another equation for a two dimensional line, only this one can handle lines of any orientation.
$$
Ax+By+C=0
$$

#### Equations for a 3D Line

The representation of a line is given by the form of a triple of equations, called the **parametric representation of a line**.
$$
x(t)=x_0+t(x_1-x_0)
y(t)=y_0+t(y_1-y_0)
z(t)=z_0+t(z_1-z_0)
$$

#### General Plane Equation

Is true for all points (x,y,z) lying on the surface of a plane.
$$
Ax+By+Cz+D=0
$$
The constants A,B,C measure the direction the plane faces, while the constant D measures the distance of the plane from the origin.

#### Sphere Equation

All points (x,y,z) on a sphere of radius r must satisfy the following equation:
$$
x^2+y^2+z^2=r^2
$$
