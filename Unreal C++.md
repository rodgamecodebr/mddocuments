# Unreal C++

### Delegates

```
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FUxtBeginGrabDelegate, UUxtGrabTargetComponent*, Grabbable, FUxtGrabPointerData, GrabPointer);
```

### UStruct (BlueprintType)

```
USTRUCT(BlueprintType)
struct UXTOOLS_API FUxtGrabPointerData
```

### Library Utility Functions 

```
UCLASS()
class UXTOOLS_API UUxtGrabPointerDataFunctionLibrary : public UBlueprintFunctionLibrary
```

### UInterface

```
UINTERFACE(BlueprintType)
class UUxtGrabTarget : public UInterface
```

### UFunction (BlueprintNativeEvent, BlueprintImplementableEvent, BlueprintCallable, BlueprintPure)

```
UFUNCTION(BlueprintNativeEvent)
	void OnBeginGrab(UUxtNearPointerComponent* Pointer);
	
void UUxtGrabTargetComponent::OnBeginGrab_Implementation(UUxtNearPointerComponent* Pointer)
```

