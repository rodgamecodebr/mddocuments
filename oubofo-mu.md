# 応募＠フォーム

### 作品名：Asai Maki (Third-Person Action Game)

### 作成時間: ～７ヶ月２０日



### 1. プログラム作品の開発環境についてご記入ください.

#### Asai Maki 

###### **OSバージョン**

​	Windows 10 Pro 64 bit 

###### ゲームエンジン

​	Unreal Engine 4.26.2

###### 使用した開発環境

​	Rider for Unreal Engine

#### Volleyball Arena

###### OSバージョン

​	Windows 10 Pro 64 bit 

###### ミドルウェア

​	DirectX 9.0c (Jun 10) / Nvidia PhysX SDK 2.8 / Dear ImGui

###### 使用した開発環境

Visual Studio Community 2019

### 2.どうしてそのプログラムを作ろうと思いましたか？

私は特別にサードパーソンアクションゲームで興味がたくさんありので、色々なゲームプレイのシステムを作ろうと思ていました。キャラクターのコントロールとかカメラとかコンボのシステムが楽しいゲームになります。

### 3.プログラムを作成する上で注意した事は？

そのプロジェクトにはブループリントとC++はゲームプレイ開発でよく使っています。例えば、カメラのシステムの実装はC++でTransformコンポネントの値を保持して、ブループリントクラスにはその値を使います。キャラクターのコンボをしながらカメラのTransformコンポネントが動きます。

### 4.プログラムを作成する上で大変だった所は？

一番大変事はNPCのAIだった。ユニットとして機能し、楽しいゲームプレイを見つけるには、アニメーションの状態と遠隔攻撃を微調整する必要がありました。

### 5. 力をいれて作った部分で、「プログラム上」で特に注意してみてもらいたい所は？

良い3Cシステムを開発したかどうか。そのプログラムはもAbility Systemを使用することをしました。

### 6. 参考にしたソースファイルがあるのなら、 どの様なところを参考にしましたか？

UE4 RPG Action Game as Reference

NinjaAbilitySystem.cpp

 またその部分のファイル名を書いてください。

-----------------------------------------------------------------------------
 プログラムを作成する上で注意した事は？
 What were you careful about?
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
 プログラムを作成する上で大変だった所は？
 What did you find difficult?
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
 力をいれて作った部分で、「プログラム上」で特に注意してみてもらいたい所は？
 What points do you want us particulary focus on your work?
-----------------------------------------------------------------------------

-----------------------------------------------------------------------------
 参考にしたソースファイルがあるのなら、 どの様なところを参考にしましたか？
 またその部分のファイル名を書いてください。
 If you took some source codes as reference, what part of them did you use? 

 Please also tell us the filename of that part.
-----------------------------------------------------------------------------

※ 注意 - Note - ※
他の人が作成したライブラリ、関数等を使っている場合は、
その部分を「別ファイル」に分けて、そのファイル名を書いてください。
If you are using a library or function created by someone else,
please put those parts in a separeted file and write the corresponding filename.

 ＜以下のような場合が該当します＞
　*　他人の作った関数を使用した場合
　*　何かを参考にし参考元のソースが半分以上残っている場合
　*　フリーのライブラリを使用した場合（ライブラリ名を明記してください。）
　*　他人の作ったライブラリを使用した場合（ライブラリ名を明記してください。）
　*　「チーム」で作成した作品の場合
　　（自分の担当した箇所やソースファイル名がわかるよう明記してください。）
　　　　　　　　For example:
　*　If you use a function made by others
　*　If you take something in reference and the referenced source represent more than half
　*　If you use a free library (please write the library name)
　*　If you use a library made by others (please write the library name)
　*　If the work is made in a team
     (please describe the source file name and points that you were in charge of)

