# C++ Knowledge from interview questions

### 1. What is the difference between class and struct?

Access modifiers

Struct member are public by default

Class members are private by default

Good practice to use class when you need an object and a struct for a simple data object

### 2. Operation between operands of different type

```cpp
#include <iostream> 

int main(int argc, char **argv) 
{    
	std::cout << 25u - 50;    
	return 0; 
}
```

The operand with lower type will be promoted to the type of the higher type operand.

From Highest to Lowest

- long Double
- double
- float
- unsigned long int
- long int
- unsigned int
- int
- char short int

### 3. Error in the code

```cpp
my_struct_t *bar;
/* ... do stuff, including setting bar to point to a defined my_struct_t object ... */
memset(bar, 0, sizeof(bar));
```

Pointer dereferencing

Compile time construct

### 4. Unary Increment and Decrement operators

```cpp
int i = 5;
int j = i++;
```

Fundamental understanding of unary operators

### 5. Alias types like size_t

```cpp
size_t sz = buf->size();
while ( --sz >= 0 )
{
	/* do something */
}
```

Fundamental understanding of types

### 6. Code optimization

```cpp
vector vec;
/* ... .. ... */
for (auto itr = vec.begin(); itr != vec.end(); itr++) {
	itr->print();
}
```

Underlying implementation of the post increment operator makes a copy of the element before incrementing

and returns the copy.

### 7. Templates

C++ 11 std library or boost library

sizeof to identify if one class is derived from another class

```cpp
template<typename D, typename B>
class IsDerivedFromHelper
{
    class No { };
    class Yes { No no[3]; };
    
    static Yes Test( B* );
    static No Test( ... );
public:
    enum { Is = sizeof(Test(static_cast<D*>(0))) == sizeof(Yes) };
    
};


template <class C, class P> 
bool IsDerivedFrom() {
    return IsDerivedFromHelper<C, P>::Is;
}
```

### 8. Inline functions

### 9. Exceptions

```cpp
#include <iostream>

class A {
public:
    A() {}
    ~A() {
        throw 42;
    }
};

int main(int argc, const char * argv[]) {
    try {
        A a;
        throw 32;
    } catch(int a) {
        std::cout << a;
    }
}
```

