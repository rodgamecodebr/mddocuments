# MRE with IPC

## Android IPC (Inter Process Communication)

Components can use IPC to communicate with a component in another app or with another component in the same app.

IPC can be:

- asynchronous - sender does not wait for the receiver to receive the message.
- synchronous - the send and receive are synchronized, and often a it appears like a normal method or function call

In order to pass messages and objects between processes, you have to generate a remote interface using AIDL, then connect to that interface through a proxy exposé in a service.

In android every application that is started runs in its own process with a unique **Process ID** or **PID**.

When an application is started , Android creates a process, spawns the main thread and starts running the main Activity.

It is possible to specify some components of the app to run on a different process than the process that started the app. Using the **process** tag.

It can be applied to activities, services, content providers and broadcast receivers and specify what process that particular component should be executed in.

### Using Intents

### Using AIDL

The Android Interface Definition Language allows you to define the programming language interface that both the client and service agree upon in order to communicate with each other using interproccess communication (IPC).

### Using Bound Services



## MRE Test Bed Source

#### Entry Point

1. Init MRTK SDK Extension API (**MREAPI**) and populate class AppsAPI with arguments

```
// MREComponent.cs
// Unity client entry point

using MixedRealityExtension.API;

MREAPI.InitializeAPI(...args...);

// MREApi.cs
// Plugin populates MREAPI.AppsAPI class with arguments
MREAPI.AppsAPI.DefaultMaterial = args[0];
MREAPI.AppsAPI.LayerApplicator = args[1];
....
```

​	2. Create a new MRTK Extension App (**MREApp**) and adds it to MRE Runtime

```
// MREComponent.cs
// Unity client
public IMixedRealityExtensionApp MREApp { get; private set; }

MREApp = MREAPI.AppsAPI.CreateMixedRealityExtensionApp(AppID, this);

// Plugin creates new MixedRealityExtensionApp passing ID and owner script
GlobalAppId = AppID;
_ownerScript = this;

// Each app has a Event Manager
EventManager = new MWEventManager(this);
// Asset loader
_assetLoader = new AssetLoader(ownerScript, this);
// RPC interface
RPC = new RPCInterface(this);
// Logger
Logger = logger ?? new UnityLogger(this);

// Adds to App Manager the new app
_apps.Add(mreApp.InstanceId, mreApp);
```

3. Define MREApp scene root (gameobject that will receive the generated model in the scene)

   ```
   // MREComponent.cs
   MREApp.SceneRoot = SceneRoot.gameObject;
   ```

4. Client sets listeners to events for MRE App

```c#
MREApp.OnConnecting += MREApp_OnConnecting;
	MREApp.OnConnectFailed += MREApp_OnConnectFailed;
	MREApp.OnConnected += MREApp_OnConnected;
	MREApp.OnDisconnected += MREApp_OnDisconnected;
	MREApp.OnAppStarted += MREApp_OnAppStarted;
	MREApp.OnAppShutdown += MREApp_OnAppShutdown;
```
5. Enable App by creating IPC connection object

   ```
   // MREComponent.cs
   // Check connection type to MRE
   // If by IPC, Client creates a new MREIPC object passing package name and service name
   
   if (ConnectionType == ConnectionType.IPC)
   {
   	MREIPC ipc = new MREIPC
       {
   		PackageName = IPCPackageName,
   		ServiceName = IPCServiceName
   	};
   	
   	//ipc is connection object
   	MREApp?.Startup(ipc);
   	
   // MixedRealityExtensionApp.cs
   // Plugin then Startups connection by subscribing listeners to events and opens connection
   
   connection.OnConnecting += Conn_OnConnecting;
   connection.OnConnectFailed += Conn_OnConnectFailed;
   connection.OnConnected += Conn_OnConnected;
   connection.OnDisconnected += Conn_OnDisconnected;
   connection.OnError += Connection_OnError;
   _conn = connection;
   
   _conn.Open();
   				
   // MREIPC.cs
   // Implements Open method of the interface Internal Connection
   
   public void Open()
   {
   	Invoke_OnConnecting();
   	
   	// MREOnEventListener is an Android Java Proxy
   	MREOnEventListener onEventListener = new MREOnEventListener();
   	
   	onEventListener.OnReceive += (type, message) =>
   	{
   		if (sentString != null && string.Compare(sentString, message) == 0) return;
   		
   		// here catches JSON message from IPC
   		Debug.Log(message);
   
   		Invoke_OnReceive(message);
   	};
   	
   	// MREOnServiceListener is an Android Java Proxy
   	MREOnServiceEventListener onServiceEventListener = new MREOnServiceEventListener();
   	
   	onServiceEventListener.OnConnected += (ipcInterface) =>
   	{
   		...
   		ipcInterface?.Call("setOnEventListener", onEventListener);
   		Invoke_OnConnected();
   	}
   	
   	// ***** the real Android IPC Plugin *****
   	_ipc = new AndroidIPC.AndroidIPC(PackageName, ServiceName);
   	...
   	_ipc.connect();
   }
   	
   // MREOnEventListener.cs
   public event Action<int, string> OnReceive;
   public override void onReceive(int type, AndroidJavaObject bundle)
   {
   	string data = bundle.Call<string>("getString", "data");
   	OnReceive?.Invoke(type, data);
   }
   
   private void Invoke_OnConnecting()
   {
   	_eventQueue.Add(() => OnConnecting?.Invoke());
   }
   
   
   // AndroidIPC.cs
   public bool connect()
   {
   	if (packageName == null || serviceName == null) return false;
       	using (AndroidJavaClass aidlServiceProvider = new AndroidJavaClass("com.sec.android.app.xr.ipc.IpcServiceProvider"))
      	{
       	return aidlServiceProvider.CallStatic<bool>("connect", _context, packageName, serviceName);
       }
   }
   ```

6. Delegates MWEventHandler handle events are added to Event Queue

   
   
7. 

8. Client sets MREApp.RPC handlers

   ```
   MREApp.RPC.OnReceive((...=>...)));
   MREApp.RPC.OnReceive((...=>... 
   {
   	MreApp.Shutdown();
   }
   )));
   ```

   