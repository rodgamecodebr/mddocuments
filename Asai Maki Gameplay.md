# Asai Maki Gameplay Demo

### Introduction

​	This project has the aim to present some gameplay features that is encountered in today 3rd person action adventure games. It is targeted to present to some employers looking for C++ gameplay programmers.

​	The engine used is Unreal 4 mainly because many AAA games are built with it.

​	ASAI MAKI artwork and elements made by third party users. ASAI MAKI Demo 2020. 
​	This demo has the sole purpose to demonstrate skills regarding to gameplay programming in terms of camera, character, input and user interface.
Game Software created by Rodrigo Reis.

### Engine Features

- UE4 Skeleton 
- Retargeting Character Animations
- Animation States
- Anim Notifies
- Sockets
- Slots
- Blend Anims
- Animation Montage
- IK Animations
- Various Input Bindings
- Collision
- Particle Systems
- Chaos
- Cel Shading Post Process
- UMG UI Interfaces
- UE4 C++ character class
- UE4 C++ interfaces

### Skeleton

- Hierarchy of bones
- Share Animations