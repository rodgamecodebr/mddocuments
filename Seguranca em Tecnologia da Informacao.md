# Seguranca em Tecnologia da Informacao

### Dinamica da Informacao e as Vulnerabilidades do Mundo Digital

#### Informacao como um ativo

- Conjunto de bens, valores e direitos que formam o patrimonio de uma empresa ou pessoa. (Capital em circulacao em determinado momento.
- O bem mais importante de uma organizacao náo é o produto ou o servico em si. Sao os conhecimentos de como se produz, quais sao as competencias e as habilidades exigidas para isso.

### Seguranca

- O grau de risco a seguranca da informacao é o resultado do conhecimento da instituicao sobre as ameacas e quanto isto representa de vulnerabilidade. Quanto menos conhecimento, maior sua vulnerabilidade.
- Meio de transmissao, forma de armazenamento e processamento

