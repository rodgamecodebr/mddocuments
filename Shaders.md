# Shaders

### What is it?	

​	small programs that runs on the GPU that has the ability to programatically control the shape, appearance, and motion of objects rendered using graphics hardware.

​	how to render an object by using programmable graphics hardware

​	operates on vertices and pixels (also called fragments) that are processed when rendering an image.

### Vertices, Pixels and Graphics Pipeline

​	a pipeline is a sequence of stages operating in parallel and in a fixed order.

​	each stage receives its input from the prior stage and sends its output to the next stage.

![](E:\mddocuments\Pictures\fig1_3.jpg)

#### Vertex Transformation

​	performs a sequence of math operations on each vertex.

​	include transforming the vertex position into a screen position for use by the rasterizer

​	generate texture coordinates for texturing

​	lighting the vertex to determine its color

#### Primitive Assembly and Rasterization

​	assembles vertices into geometric primitives based on the geometric primitive batching information that accompanies the sequence of vertices.

​	these primitives may require clipping to view frustum (the view's visible region of 3D space)

​	the rasterizer may discard polygons based on whether they face forward or backward in a process called as culling.

​	rasterization is the process of determining the set of pixels covered by a geometric primitive

​	the results of rasterization are a set of pixel locations as well as a set of fragments.

![](E:\mddocuments\Pictures\fig1_4.jpg)



#### Interpolation, Texturing and Coloring

​	performs a sequence of texturing and math operations, and determines a final color for each fragment.

#### Raster Operations

​	performs a final sequence of per-fragment operations before updating the frame buffer.

​	the raster operations stage checks each fragment based on a number of tests, including the scissor, alpha, stencil, and depth tests.

​	these tests involve the fragment's color or depth, the pixel location, and per-pixel values such as the depth value and stencil value of the pixel.

![](E:\mddocuments\Pictures\fig1_5.jpg)



### The Graphics Pipeline

![](E:\mddocuments\Pictures\fig1_6.jpg)



### The Programmable Graphics Pipeline

​	the dominant trend in graphics hardware design today is the effort to expose more programmatically within the GPU.

![](E:\mddocuments\Pictures\fig1_7.jpg)



#### The Programmable Vertex Processor

​	Begins by loading each vertex's attributes (position, color, texture coordinates and so on) into the vertex processor.

​	Vertex Processor repeatedly fetches the next instruction and executes it until the vertex program terminates.

​	Instructions access several distinct sets of registers banks that contain vector values, such as position, normal or color.

​	Vertex attribute registers are read-only and contain the application-specified set of attributes for the vertex.

​	Temporary registers can be read and written and are used for computing intermediate results.

​	The output results registers are write-only.

​	When the vertex program terminates, the output result registers contain the newly transformed vertex.

![](E:\mddocuments\Pictures\fig1_8.jpg)

​	Some of the vertex processing operations:

- Floating-point vectors of two, three and four components are necessary
- Add
- Multiply
- Multiply-Add
- Dot Product
- Minimum
- Maximum

#### The Programmable Fragment Processor

​	Require many of the same math operations as programmable vertex processors do, but they also support texturing operations.

​	Involves executing a sequence of instructions until the program terminates

​	Read only input registers contain interpolated per-fragment parameters derived from the per-vertex parameters of the fragment's primitive

​	Read/Write temporary registers store intermediate values

​	Write operations to write-only output registers become the color and optionally the new depth of the fragment

​	Fragment program instructions include texture fetches

![](E:\mddocuments\Pictures\fig1_9.jpg)

