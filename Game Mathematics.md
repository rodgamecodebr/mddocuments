# Game Mathematics

### Set Theory and Functions

A set is a conceptual container that allows you to group any number of unique concepts together.

Sets contain other concepts

Each item in a set must be unique

Items in sets are unordered

### Set Notation

Explicit Notation

K = {a,b,c,d}

B = {Cairo, Paris, Tokyo}

Set-Builder Notation

Q = {x | x is an integer}

L = {z:z is a dinosaur}

### Common Sets

Null Set

Real Numbers : R

Integers: Z

Positive/Negative integers: Z+-

Rationals: Q

Natural Integers: N

### Set Relational Operators

Item Containment 

Equality  

Subset 

Superset 

### Algebra Set Theory

Magnitude

Complement

Union

Intersection

Difference

### Interval Notation

Easy way to specify sets of a continuous range of real numbers

[a,b] = {x | a <= x <= b}

### Absolute Value

The absolute value of a number is its distance from zero.

|a| = a

|-a| = a

### Mathematical Functions

Function is a mapping from a set A (domain) to a set B (the range) such that with every element in A, the function associates one element in B.

Function notation usually designated by small lower cases letters: f,g,h

example: f(x)

The elements in the range passed to the function are called the parameters or arguments of the function

N-Tuples - list of ordered items (x1,x2,x3,...xn)

2-tuples are used to represent 2d points

3-tuples are used for 3d points

### Inverse Functions

An inverse function undoes the operation of the function, sending elements in the range back into the domain.

Not all functions have inverses.



## Mathematical Functions

is a function that maps from one mathematical set to another.

f(x) = (x,-x)

Q(x,y,z) = x+y+z

### Graphs to visualize functions

Cartesian Coordinate Systems

- Two dimensional
- Three dimensional

### Fundamental Mathematical Functions

Absolute Value Function

|x| = +sqrt(x)

Exponential Functions - are good to at modelling parameters that increase/decrease

- Have steep graphs
- exponential functions with base e(2.71828)
- applications (intensity of fog, life of particles, population growth)
- choose a general enough exponential function
- impose a number of constraints equal to number of contants
- Solve for the constants

Logarithmic Functions - are inverse of exponential functions

- Grow slowly
- natural log function ln(x)
- applications: used to model slow-growing behavior, used to solve exponential equations