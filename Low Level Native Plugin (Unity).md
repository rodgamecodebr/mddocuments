# Low Level Native Plugin (Unity)

Used mostly to implement low-level rendering and work with multithreaded rendering.

### Interface Registry

Should export :

- UnityPluginLoad

  ```
   // Unity plugin load event extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API    UnityPluginLoad(IUnityInterfaces* unityInterfaces) 
  {
  }
  ```

- UnityPluginUnload

  ```
  // Unity plugin unload event extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API    UnityPluginUnload()
  { 
  }
  ```

### Access to Graphics Device

Access to graphics device by getting **IUnityGraphics** interface.

```
static IUnityInterfaces* s_UnityInterfaces = NULL; static IUnityGraphics* s_Graphics = NULL; 

 UnityPluginLoad(IUnityInterfaces* unityInterfaces)
 {    
 	s_UnityInterfaces = unityInterfaces;   
 	s_Graphics = unityInterfaces->Get<IUnityGraphics>(); 
 }
```

### Plugin Callbacks on the Rendering Thread

To start your plugin doing rendering , you should call it from the GL.IssuePluginEvent command. As Unity rendering is multithreaded, it is not always possible that the rendering will process immediately, it might interfere in what the rendering thread is working on at the moment.

Signature for Unity callbacks on rendering events is found in IUnityGraphics.h

```
 // Plugin function to handle a specific rendering event static void UNITY_INTERFACE_API OnRenderEvent(int eventID) 
 {    
 	//TODO: user rendering code 
 } 
 
 // Freely defined function to pass a callback to plugin-specific scripts
extern "C" UnityRenderingEvent UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API
    GetRenderEventFunc()
{
    return OnRenderEvent;
}
```

```
#if UNITY_IPHONE && !UNITY_EDITOR 
[DllImport ("__Internal")] 
#else 
[DllImport("RenderingPlugin")] 
#endif 

private static extern IntPtr GetRenderEventFunc();     // Queue a specific callback to be called on the render thread GL.IssuePluginEvent(GetRenderEventFunc(), 1); 
```

