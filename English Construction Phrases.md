# English Construction Phrases

#### Deal with

We deal with all unity versions released to the public.

I have to deal with all gameobjects that are going to consume the unity scene.

#### Unblocked

Get projects unblocked and be more easy to develop for developers.

#### I've seen

I've seen many projects that are going in the wrong direction.

I have seen many errors in the candidate application source code and I would like him to fix these issues and resend his work to us.

#### Architecture toolbox

It is essential that all developers has some skills in designing API before so that everyone can give advice about the application architecture using their own architecture toolbox past experiences.

#### Walk through

I'd like to walk through all chapters that is also contained in the book but here we are going to discuss the details further so that everyone can see the big picture.

#### I've got

I've got a nice application presenting all aspects of optimization that we covered so far.

#### Let's start by

Let's start by gathering all evidence we found and we will compare all new data against the old one.

#### Live in the

They are all live in the same source code file and that's why it is important that we ensure all syntax is error-free.

#### End up

A lot of projects end up being very disorganized and with a lot of unused content stored in the root folder of project.

#### Build up

So basically one year ago exactly I was trying to build up a C++ game engine application where I could employ good design patterns and using graphics API like DirectX.

#### What I mean by that exactly

I started to work with scriptable objects in Unity so that I can reuse modular parts of the assets in the game. What I mean by that exactly is that I can reference one container for all types of assets that the game will have and according to the application context it will load the correct asset.

#### But the thing is...

I can see that most developers wants to work in the game industry but the thing is that most of them in fact don't know what it takes to achieve a great level of knowledge and experience in order to call attention of the AAA studios.

#### Run into

Throughout my development journey I ran into many problems related to optimization of the assets of the game.

#### Get Lost

All my changes I made were completely got lost because I forgot to mention that you cannot modify things when the play button is active.