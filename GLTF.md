# GLTF

### What is it?

Stands for **GL Transmission Format** 

It is a royalty-free specification for the efficient transmission and loading of 3D scenes and models by engines and applications.

GLTF minimizes the size of 3D assets, and the runtime processing needed to unpack and use them.

**GLTF is the JPEG of 3D.**

### glTF 2.0 Scene Description

- .gltf (JSON) - Node hierarchy, PBR material textures, cameras. Describes the structure and composition of a scene containing 3d models.

- .bin - Geometry(vertices and indices), Animation (key-frames), Skins (inverse-bind matrices)

- .png, .jpg, .ktx2 Textures

### Scene, Nodes

- GLTF JSON may contain scenes.
- Each scene can contain an array of indices of nodes
- Each of the nodes can contain an array of indices of its children
- A node may contain a local transform column-major matrix array (translation, rotation, scale)
- Each node may refer to a **mesh** or a **camera**, **vertex skinning**

### Meshes

Meshes may contain multiple mesh primitives, that is, the geometry data for rendering the mesh.

### Animations

### Buffers, Buffer Views, Accessors

Contain the data that is used for the geometry of 3d models, animations and skinning.

Buffer Views add structural information to this data.

Accessors define the exact type and layout of the data.

### Materials

Each mesh primitive may refer to one of the materials that are contained in a GLTF asset.

The materials describe how an object should be rendered, based on physical material properties.

### Binary GLTF files

Binary data 

### Extensions

GLTF extensions extend the base GLTF model format. 

Extensions can introduce new properties (including properties that reference external data, and the extension can define the format of those data), new parameter semantics, reserved IDs, and new container formats.

GLTF format allows extension to add new functionality or to simplify the definition of commonly used properties.

It has to be listed in the top-level **extensionsUsed** property.

```json
"extensionsUsed" : [
    "KHR_draco_mesh_compression"
]
```

The **extensionsRequired** property lists the extensions that are strictly required to properly load the asset.

```json
"extensionsRequired" : [
    "KHR_draco_mesh_compression"
]
```

Extensions allow adding arbitrary objects in the **extensions** property of other objects.

