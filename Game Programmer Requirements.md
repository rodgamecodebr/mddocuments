# Game Programmer Requirements

### Skills

- C++ strong programming ability
- Understanding of 3D math
- Strong and effective communication
- Problem solving, keen eye for details
- Passion to develop outstanding games