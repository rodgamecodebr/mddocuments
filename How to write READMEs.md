# How to write READMEs

### Title and Description

Capture the spirit of your project clearly.

### Pre requisites

Include any information that is necessary to run or work with project's code

- Additional dependencies
- Library
- Installation instructions
- Common usage
- Known bugs
- Usage code

### Important questions

What steps need to be taken?

What should the user already have installed or configured?

What might they have a hard time understanding right away?

### Contributing

### License

### Code status

### Known Bugs

### FAQ

### Table of Contents